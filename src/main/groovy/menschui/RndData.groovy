package menschui


import groovy.transform.TypeChecked


@TypeChecked
class RndData {

    static final String windows1252encoding = "windows-1252"
    static final String utf8Encoding = "UTF-8"

    static String testdataDir = "./src/main/resources/testdata"

    static final String newLine = "\n"
    static final String tab = "\t"
    static Random random = new Random(System.nanoTime())

    String[] vornamenW, vornamenM, nachnamen, strassen, plzOrte

    String getRandomValue(String[] arr) {
        arr[random.nextInt(arr.size())]
    }

    String getRandomStr(int length) {
        StringBuffer buf = new StringBuffer()
        1.upto(length) {
            Character c = (65 + random.nextInt(26)) as Character
            buf.append(c)
        }
        buf.toString()
    }

    String getRandomID() { random.nextInt(1000000).toString().padLeft(6, "0") }

    String[] getFromFile(String name) {
        new File("$testdataDir/${name}.txt").getText(utf8Encoding).split(newLine)*.trim() as String[]
    }

    String getRndVornameW() {
        if (!vornamenW) {
            vornamenW = getFromFile("vornamenw")
        }
        getRandomValue(vornamenW)
    }

    String getRndVornameM() {
        if (!vornamenM) {
            vornamenM = getFromFile("vornamenm")
        }
        getRandomValue(vornamenM)
    }

    String getRndGeschlecht() {
        String[] geschlechter = ["M", "W"]
        getRandomValue(geschlechter)
    }

    String getRndVorname(String geschlecht) {
        geschlecht.toLowerCase() == "m" ? rndVornameM : rndVornameW
    }

    String getRndHausnummer() {
        intValueBetween(111, 999).toString().substring(0, intValueBetween(1, 3))
    }

    String getRndNachname() {
        if (!nachnamen) {
            nachnamen = getFromFile("nachnamen")
        }
        getRandomValue(nachnamen)
    }

    int intValueBetween(int from, int to) { from + random.nextInt(to - from + 1) }

    String getRndTelefon() {
        int vorwahl = intValueBetween(1111, 9999)
        int hauptwahl = intValueBetween(1111111, 9999999)
        "0$vorwahl/$hauptwahl"
    }

    String getRndMobil() {
        int vorwahl = intValueBetween(179, 181)
        int hauptwahl = intValueBetween(1111111, 9999999)
        "0$vorwahl/$hauptwahl"
    }

    String getRndDatum(int vonJahr, int bisJahr) {
        def yyyy = intValueBetween(vonJahr, bisJahr)
        def dd = 1 + random.nextInt(28)
        dd = dd.toString().padLeft(2, "0")
        def MM = 1 + random.nextInt(12)
        MM = MM.toString().padLeft(2, "0")
        "${dd}.${MM}.${yyyy}"
    }

    String getRndStrasse() {
        if (!strassen) {
            strassen = getFromFile("strassen")
        }
        getRandomValue(strassen)
    }

    String[] getRndPlzOrt() {
        def plzOrt = new String[2]
        if (!plzOrte) {
            plzOrte = getFromFile("plzorte")
        }
        def plzOrtArr = getRandomValue(plzOrte).split(tab)*.trim()
        plzOrt[0] = plzOrtArr[0]
        plzOrt[1] = plzOrtArr[2]
        plzOrt
    }

}
