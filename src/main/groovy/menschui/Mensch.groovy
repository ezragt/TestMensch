package menschui

class Mensch {

    String name
    String wohnort
    String strasse
    String hausnummer
    int id

    Mensch() {
        id = 0
        name = ""
        wohnort = ""
        strasse = ""
        hausnummer = ""
    }

    Mensch(String name, String wohnort, String strasse, String hausnummer, int id) {
        this.name = name
        this.wohnort = wohnort
        this.strasse = strasse
        this.hausnummer = hausnummer
        this.id = id
    }

    //TODO: benötigt?
    Mensch(String name, String wohnort, String strasse, String hausnummer) {
        this.name = name
        this.wohnort = wohnort
        this.strasse = strasse
        this.hausnummer = hausnummer
    }

    //TODO: isEqual
    boolean vergleiche(Mensch mensch) {
        if (mensch.id == id && mensch.name == name && mensch.wohnort == wohnort && mensch.strasse == strasse && mensch.hausnummer == hausnummer) {
            return true
        } else {
            return false
        }
    }

    public String toString() {
        "{ id= $id, name= '$name', strasse= '$strasse', hausnummer= '$hausnummer', wohnort= '$wohnort' }"
    }

    public String getCsv() {
        ""
    }

}
