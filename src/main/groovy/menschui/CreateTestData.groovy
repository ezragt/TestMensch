package menschui

class CreateTestData {

    final static boolean recreateTable= false

    static RndData rnd = new RndData()

    static db = new Datenbank()

    static void addMenschenToDatabase(int limit) {
        int count= db.getNewId() - 1
        println "count= $count ; limit= $limit"
        while (count < limit) {
            count= addSomeMenschenToDatabase(100)
            println "count= $count"
        }
    }

    static int addSomeMenschenToDatabase(int count) {
        int start= db.getNewId()
        int n= start
        StringBuffer buf= new StringBuffer()
        buf.append("""INSERT INTO users (id, name, wohnort, strasse, hausnummer) VALUES\n""")
        start.upto(start + count - 1) {
            def mensch= randomMensch()
            while(mensch.strasse.length() > 59) {
                mensch= randomMensch()
            }
            mensch.id= it
            buf.append(getInsertString(mensch))
            buf.append("\n")
            n= it
        }
        buf.replace(buf.length()-2, buf.length(), ";")
        try {
            println "running insert into users ..."
            db.sql.execute(buf.toString())
            println "... done."
        } catch(Exception e) {
            println "... not done."
            return start - 1
        }

        return n
    }

    static String getInsertString(Mensch mensch) {
        mensch.with {
            "($id, '$name', '$wohnort', '$strasse', '$hausnummer'),"
        }
    }

    static Mensch randomMensch() {
        String vorname= (rnd.intValueBetween(0, 1) == 1) ? rnd.getRndVornameM() :  rnd.getRndVornameW()
        String nachname= rnd.getRndNachname()

        def plzOrt= rnd.getRndPlzOrt()
        def ort= plzOrt[0] + " " + plzOrt[1]

        new Mensch(
                name: "$vorname $nachname",
                wohnort: ort,
                strasse: rnd.getRndStrasse(),
                hausnummer: rnd.intValueBetween(1, 300)
        )
    }

    static void main(String[] args) {
        if (recreateTable) {
            db.dropTable()
            db.createTableIfNotExists()
        }
        addMenschenToDatabase(3000000)
    }

}
