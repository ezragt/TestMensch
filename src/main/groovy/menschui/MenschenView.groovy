package menschui

import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox

class MenschenView {

    //TODO: 1?
    MenschenController controller
    Scene scene1
    Suche suche = new Suche(this)
    Bottom bottom
    DataTable dataTable = new DataTable(controller)
    int scene1Width = 910
    int scene1Height = 550

    MenschenView(MenschenController controller) {
        this.controller = controller
        bottom = new Bottom(controller, this)

        StackPane titel = new StackPane()
        Label titelText = new Label("Verwaltung")
        titel.getChildren().addAll(titelText)

        VBox layout1 = new VBox(20)
        layout1.getChildren().addAll(titel, suche.getSuche(), dataTable.getTable(), bottom.getBottom())

        scene1 = new Scene(layout1, scene1Width, scene1Height)
    }

    void addMensch(Mensch mensch) {
        dataTable.data.add(mensch)
        dataTable.table.sort()
    }

    void addAllMenschen(List<Mensch> mensch) {
        dataTable.data.addAll(mensch)
    }

    Mensch getSelectedItem() {
        if (dataTable.table.getSelectionModel().getSelectedItem() as Mensch != null) {
            Mensch selectedMensch = dataTable.table.getSelectionModel().getSelectedItem() as Mensch
            return selectedMensch
        } else {
            Mensch mensch1 = new Mensch()
            return mensch1
        }
    }

    void deleteTable() {
        dataTable.table.getItems().clear()
    }

    // TODO: benotogt? L�schen
    void setData(List menschen) {
        ObservableList items = new ObservableList(menschen)
        dataTable.data
    }

    void suche(Mensch suche) {
        controller.ladeSuche(suche)
    }
}
