package menschui

class MenschenController {

    Datenbank datenbank
    Mensch mensch
    MenschenView menschenView

    MenschenController(Datenbank datenbank) {
        this.datenbank = datenbank
    }

    int getNewId() {
        return datenbank.getNewId()
    }

    void createMensch(Mensch mensch) {
        this.mensch = mensch
        // TODO: Logging
        println "${this.class.name}: create(): $mensch"
        datenbank.createMensch(mensch)
        menschenView.addMensch(mensch)
    }

    void updateMensch(Mensch mensch) {
        println "${this.class.name}: update(): $mensch"
        this.mensch = mensch
        datenbank.updateMensch(mensch)
        reloadTable()
    }

    // TODO: k�nnte wegrationalisiert werden
    void ladeDaten() {
        Mensch mensch1 = new Mensch()
        ladeSuche(mensch1)
    }

    // TODO: searchMenschen
    void ladeSuche(Mensch sucheMensch) {
        def menschen = datenbank.getMenschenByMensch(sucheMensch)
        //menschenView.setData(menschen)
        menschenView.deleteTable()
        menschenView.addAllMenschen(menschen)
    }

    void reloadTable() {
        menschenView.deleteTable()
        ladeDaten()
    }

    void deleteMensch(Mensch mensch) {
        println "${this.class.name}: delete(): $mensch"
        datenbank.deleteMensch(mensch)
        reloadTable()
    }

}