package menschui

import UiElements.Spacer
import UiElements.UiLabel
import UiElements.UiTextfield
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.layout.*
import javafx.stage.Stage

class MenschEditView {

    static int createDialogWidth = 910
    static int createDialogHeight = 350

    MenschenController controller

    final Mensch standartMensch = new Mensch()
    Mensch mensch = new Mensch()

    HBox menschAenderungsBox = new HBox()

    VBox aenderung = new VBox()
    VBox menschEigenschaften = new VBox()
    VBox buttons = new VBox()

    boolean isNew = false

    class LabelEntry {
        UiTextfield textfield
        Label label
        Spacer superSpacer = new Spacer(300, 30)

        LabelEntry(String name, String value) {
            textfield = new UiTextfield(name)
            textfield.setText(value)
            if (value != "null" && value != "0") {
                label = new UiLabel(value)
                menschEigenschaften.getChildren().addAll(spacer30y(), label)
            } else {
                menschEigenschaften.getChildren().addAll(spacer30y(), superSpacer)
            }
            aenderung.getChildren().addAll(spacer30y(), textfield)
        }
    }

    MenschEditView(MenschenController controller) {
        this.controller = controller
    }

    //TODO: namen
    void display(Mensch preMensch = standartMensch) {
        Stage createMenschDialog = new Stage()
        createMenschDialog.setTitle("create Mensch")

        if (preMensch.id == 0) {
            int newId = controller.getNewId()
            isNew = true
            preMensch.id = newId
        }

        //TODO: Doppel-Quotes "" n�tigt?
        LabelEntry idLE = new LabelEntry("id", "" + preMensch.id)
        idLE.textfield.editable = false
        LabelEntry nameLE = new LabelEntry("name", "" + preMensch.name)
        LabelEntry wohnortLE = new LabelEntry("wohnort", "" + preMensch.wohnort)
        LabelEntry strasseLE = new LabelEntry("strasse", "" + preMensch.strasse)
        LabelEntry hausnummerLE = new LabelEntry("hausnummer", "" + preMensch.hausnummer)

        Button okButton = new Button("Ok")
        okButton.setOnAction { e ->
            mensch.id = Integer.parseInt(idLE.textfield.getText())
            mensch.name = nameLE.textfield.getText() + ""
            mensch.wohnort = wohnortLE.textfield.getText() + ""
            mensch.strasse = strasseLE.textfield.getText() + ""
            mensch.hausnummer = hausnummerLE.textfield.getText() + ""

            //TODO: name?
            if (!preMensch.vergleiche(mensch)) {
                if (isNew) {
                    controller.createMensch(mensch)
                } else {
                    controller.updateMensch(mensch)
                }
            }
            createMenschDialog.close()
        }
        okButton.setPrefSize(100, 30)
        Button abbruchButton = new Button("Abbruch")
        abbruchButton.setOnAction { e ->
            createMenschDialog.close()
        }
        abbruchButton.setPrefSize(100, 30)
        HBox buttonShell = new HBox()
        buttonShell.getChildren().addAll(spacer50x(), okButton, spacer50x(), abbruchButton, spacer50x())
        buttons.getChildren().addAll(spacer270y(), buttonShell)

        menschAenderungsBox.getChildren().addAll(menschEigenschaften, aenderung, buttons)

        VBox layout = new VBox()
        layout.setPrefSize(createDialogWidth, createDialogHeight)
        layout.getChildren().addAll(menschAenderungsBox)

        Scene scene2 = new Scene(layout)
        createMenschDialog.setScene(scene2)
        createMenschDialog.show()
    }

    static Region spacer30y() {
        Spacer spacer = new Spacer(0, 30)
        return spacer
    }

    static Region spacer270y() {
        Spacer spacer = new Spacer(0, 270)
        return spacer
    }

    static Region spacer50x() {
        Spacer spacer = new Spacer(50, 0)
        return spacer
    }
}
