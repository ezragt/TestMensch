package menschui;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

//TODO: ben�tigt?
public class AddMoreTableDataOnScrollToBottom extends Application {

    @Override
    public void start(Stage primaryStage) {
        TableView<Item> table = new TableView<>();
        table.getColumns().add(column("Item", Item::nameProperty));
        table.getColumns().add(column("Value", Item::valueProperty));


        addMoreData(table, 20);

        Scene scene = new Scene(new BorderPane(table), 400, 400);
        primaryStage.setScene(scene);
        primaryStage.show();

        ScrollBar verticalBar = (ScrollBar) table.lookup(".scroll-bar:vertical");
        verticalBar.valueProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue.doubleValue() >= verticalBar.getMax()) {
                addMoreData(table, 20);
            }
        });
    }

    private void addMoreData(TableView<Item> table, int numItems) {
        Task<List<Item>> dataRetrieveTask = new Task<List<Item>>() {
            @Override
            public List<Item> call() throws Exception {
                // mimic connect to db:
                Thread.sleep(500);
                List<Item> items = new ArrayList<>();
                int nextItem = table.getItems().size() + 1;
                for (int i = nextItem; i < nextItem + numItems; i++) {
                    items.add(new Item("Item " + i, i));
                }
                return items;
            }
        };
        dataRetrieveTask.setOnSucceeded(e -> table.getItems().addAll(dataRetrieveTask.getValue()));
        new Thread(dataRetrieveTask).start();
    }

    private <S, T> TableColumn<S, T> column(String title, Function<S, ObservableValue<T>> prop) {
        TableColumn<S, T> col = new TableColumn<>(title);
        col.setCellValueFactory(cellData -> prop.apply(cellData.getValue()));
        return col;
    }

    public static class Item {

        private final StringProperty name = new SimpleStringProperty();
        private final IntegerProperty value = new SimpleIntegerProperty();

        public Item(String name, int value) {
            setName(name);
            setValue(value);
        }

        public final StringProperty nameProperty() {
            return this.name;
        }


        public final String getName() {
            return this.nameProperty().get();
        }


        public final void setName(final String name) {
            this.nameProperty().set(name);
        }


        public final IntegerProperty valueProperty() {
            return this.value;
        }


        public final int getValue() {
            return this.valueProperty().get();
        }


        public final void setValue(final int value) {
            this.valueProperty().set(value);
        }


    }

    public static void main(String[] args) {
        launch(args);
    }
}