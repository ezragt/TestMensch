package menschui

import javafx.scene.control.*
import javafx.scene.layout.*

class Bottom {

    private MenschenController controller
    private MenschenView menschenView

    HBox bottom = new HBox()

    Bottom(MenschenController controller, MenschenView menschenView) {
        this.controller = controller
        this.menschenView = menschenView

        int spacerwidth = (1 / 7 * MenschenMain.scene1Width).intValue()

        Button create = new Button("Create")
        create.setOnAction { e ->
            MenschEditView createMenschDialog = new MenschEditView(controller)
            createMenschDialog.display()
        }
        create.setPrefWidth(spacerwidth)

        Button delete = new Button("Delete")
        delete.setOnAction { e ->
            controller.deleteMensch(menschenView.getSelectedItem())
        }
        delete.setPrefWidth(spacerwidth)

        Button update = new Button("Update")
        update.setOnAction { e ->
            MenschEditView createMenschDialog = new MenschEditView(controller)
            createMenschDialog.display(menschenView.getSelectedItem())
        }
        update.setPrefWidth(spacerwidth)

        bottom.getChildren().addAll(
                MenschenMain.spacer(spacerwidth, 1),
                create,
                MenschenMain.spacer(spacerwidth, 1),
                update,
                MenschenMain.spacer(spacerwidth, 1),
                delete,
                MenschenMain.spacer(spacerwidth, 1),)
    }

    HBox getBottom() {
        return bottom
    }
}
