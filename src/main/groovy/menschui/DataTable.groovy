package menschui

import groovy.transform.CompileStatic
import javafx.beans.value.ObservableValue
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.control.*
import javafx.scene.control.cell.PropertyValueFactory
import javafx.util.Callback

@CompileStatic
class DataTable {

    ObservableList<Mensch> data = FXCollections.observableArrayList()

    private TableView table
    private ScrollPane scrollPane
    MenschenController controller

    DataTable(MenschenController controller) {
        this.controller = controller
        table = new TableView()

        scrollPane = new ScrollPane(table)
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER)
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED)

        TableColumn id = addColumn("Id")
        TableColumn name = addColumn("Name")
        TableColumn wohnort = addColumn("Wohnort")
        TableColumn strasse = addColumn("Strasse")
        TableColumn hausnummer = addColumn("Hausnummer")
        table.getSortOrder().add(id)
        table.setItems(data)
        table.setEditable(true)
    }


    private TableColumn addColumn(String columnName) {
        TableColumn column = new TableColumn(columnName)
        column.setCellValueFactory(
                new PropertyValueFactory<Mensch, String>(columnName) as Callback<TableColumn.CellDataFeatures, ObservableValue>
        )
        column.setPrefWidth(170)
        table.getColumns().add(column)
        return column
    }

    TableView getTable() {
        return table
    }

}
