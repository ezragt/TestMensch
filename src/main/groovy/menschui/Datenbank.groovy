package menschui

import groovy.sql.Sql

class Datenbank {

    final boolean isDatabaseLocal = false

    def sql

    Datenbank() {
        try {
            //TODO: Configurable
            if (isDatabaseLocal) {
                sql = Sql.newInstance("jdbc:mysql://localhost:3306/name", "admin", "admin", "com.mysql.cj.jdbc.Driver")
            } else {
                sql = Sql.newInstance("jdbc:postgresql://joe:5432/webshop", "webshop", "123456", "org.postgresql.Driver")
            }
        } catch (Exception e) {
            //TODO: UI - Fehlermeldung
            println "Eine Verbindung zur DB ist nicht möglich. Bitte prüfen Sie die Verbindungsdaten."
            println e.getStackTrace().join("\n")
            System.exit(-1)
        }
        createTableIfNotExists()
    }

    void deleteMensch(Mensch mensch) {
        sql.execute('delete from users where id = ?', [mensch.id])
    }

    void createMensch(Mensch mensch) {
        sql.execute('insert into users values(?,?,?,?,?)', eigenschaftenMensch(mensch))
    }

    static List eigenschaftenMensch(Mensch mensch) {
        List eigenschaften = [mensch.id, mensch.name, mensch.wohnort, mensch.strasse, mensch.hausnummer]
        return eigenschaften
    }

    Integer getNewId() {
        int neueId = getMaxId()
        return neueId == 0 ? 1 : neueId + 1
    }

    void updateMensch(Mensch mensch) {
        sql.executeUpdate('update users set id=?,name=?,wohnort=?,strasse=?,hausnummer=? where id= ?', eigenschaftenMensch(mensch) + mensch.id)
    }

    Integer getMaxId() {
        def row = sql.firstRow("select max(id) as id from users")
        return row?.id ?: 0
    }

    List<Mensch> getMenschenByMensch(Mensch sucheMensch) {
        List<Mensch> menschen = new ArrayList<>()
        String id
        if (sucheMensch.id != 0) {
            id = sucheMensch.id
        } else {
            id = ""
        }
        sql.eachRow(
                """select * from users where 
                   cast(id as varchar) like ?
                   and name like ?
                   and wohnort like ?
                   and strasse like ? 
                   and hausnummer like ?
                   limit 1000""",
                ['%' + id + '%',
                 '%' + sucheMensch.name + '%',
                 '%' + sucheMensch.wohnort + '%',
                 '%' + sucheMensch.strasse + '%',
                 '%' + sucheMensch.hausnummer + '%']) {
            row ->
                Mensch mensch = new Mensch(row.name, row.wohnort, row.strasse, row.hausnummer, row.id)
                menschen.add(mensch)
        }

        return menschen
    }

    void createTableIfNotExists() {
        sql.execute("""
            create table if not exists users (
                id INT NOT NULL, 
                name VARCHAR(60) NOT NULL, 
                wohnort VARCHAR(60) NOT NULL, 
                strasse VARCHAR(60), 
                hausnummer VARCHAR(10) NOT NULL, 
                PRIMARY KEY(id))
        """)
    }

    void dropTable() {
        sql.execute('drop TABLE users')
    }

}
