package menschui

import groovy.transform.CompileStatic
import javafx.scene.layout.*
import javafx.stage.Stage
import javafx.application.Application

@CompileStatic
class MenschenMain extends Application {

    MenschenController controller
    Datenbank datenbank
    Stage stage
    MenschenView menschenView
    static int scene1Width = 910

    @Override
    void init() throws Exception {
        datenbank = new Datenbank()
        controller = new MenschenController(datenbank)
        menschenView = new MenschenView(controller)
        controller.menschenView = menschenView
        controller.ladeDaten()
    }

    @Override
    void start(Stage primaryStage) {
        stage = primaryStage
        stage.setTitle("Mensch Verwaltung")
        stage.setScene(menschenView.scene1)
        stage.show()
    }

    static Region spacer(int width, int height) {
        Region spacer = new Region()
        spacer.setPrefSize(width, height)
        VBox.setVgrow(spacer, Priority.ALWAYS)
        return spacer
    }
}