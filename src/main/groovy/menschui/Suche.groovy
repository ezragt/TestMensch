package menschui

import UiElements.UiTextfield
import javafx.scene.control.Button
import javafx.scene.layout.HBox

class Suche {

    HBox topMenu = new HBox()
    MenschenView menschenView

    Suche(MenschenView menschenView) {
        this.menschenView = menschenView
        UiTextfield id = new UiTextfield("id")
        UiTextfield name = new UiTextfield("name")
        UiTextfield wohnort = new UiTextfield("wohnort")
        UiTextfield strasse = new UiTextfield("strasse")
        UiTextfield hausnummer = new UiTextfield("hausnummer")
        Button suche = new Button("Suche")
        suche.setPrefSize(200, 0)

        suche.setOnAction { e ->

            Mensch gesucht = new Mensch()

            if (id.getText() != "") {
                try {
                    gesucht.id = Integer.parseInt(id.getText())
                } catch (Exception e2) {
                    gesucht.id = 0
                }

            }
            gesucht.name = name.getText()
            gesucht.wohnort = wohnort.getText()
            gesucht.strasse = strasse.getText()
            gesucht.hausnummer = hausnummer.getText()
            menschenView.suche(gesucht)
        }

        topMenu.getChildren().addAll(id, name, wohnort, strasse, hausnummer, suche)
    }

    HBox getSuche() {
        return topMenu
    }

}
