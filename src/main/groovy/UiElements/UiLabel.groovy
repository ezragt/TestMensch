package UiElements

import javafx.scene.control.Label


class UiLabel extends Label{


    UiLabel(String value) {
        super(value)
        setPrefSize(300, 30)
    }

}
