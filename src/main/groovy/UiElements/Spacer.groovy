package UiElements

import javafx.scene.layout.Region

class Spacer extends Region {

    Spacer(int x, int y){
        setMinSize(x,y)
    }

}
