package UiElements

import javafx.scene.control.TextField

class UiTextfield extends TextField {

    UiTextfield(String value) {
            setPromptText(value)
            focusTraversable= false
            setPrefSize(300, 30)
    }
}
