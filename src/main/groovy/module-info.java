module menschgui {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.apache.groovy;
    requires java.sql;
    requires org.apache.groovy.sql;


    opens menschui to javafx.fxml;
    exports menschui;
    exports UiElements;

}